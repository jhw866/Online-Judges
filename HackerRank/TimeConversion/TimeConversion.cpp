#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

int main(){
    string time;
    cin >> time;
    
    int hour = stoi(time.substr(0, 2));
      
    if (time.at(time.size() - 2) == 'P' && hour < 12) {
        hour += 12;
    }
    
    if (time.at(time.size() - 2) == 'A' && hour == 12){
        hour = 0;
    }
    
    if (hour < 10)
        cout << "0";
    cout << hour << time.substr(2, time.size() - 4) << endl;
    
    return 0;
}

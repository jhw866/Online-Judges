#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int sumHourGlass(const vector<vector<int> >& arr, int row, int col) {
    int sum = arr.at(row).at(col);
    sum += arr.at(row).at(col + 1);
    sum += arr.at(row).at(col + 2);
    
    sum += arr.at(row + 1).at(col + 1);
    
    sum += arr.at(row + 2).at(col);
    sum += arr.at(row + 2).at(col + 1);
    sum += arr.at(row + 2).at(col + 2);
    
    return sum;
}

int main(){
    vector< vector<int> > arr(6,vector<int>(6));
    for(int arr_i = 0;arr_i < 6;arr_i++){
       for(int arr_j = 0;arr_j < 6;arr_j++){
          cin >> arr[arr_i][arr_j];
       }
    }
    int biggestSum = sumHourGlass(arr, 0, 0);
    
    for (int row = 0; row < 4; ++row) {
        for (int col = 0; col < 4; ++col) {
            int sum = sumHourGlass(arr, row, col);
            if (biggestSum < sum)
                biggestSum = sum;
        }
    }
    
    cout << biggestSum << endl;
    
    return 0;
}


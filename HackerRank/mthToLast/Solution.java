import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        
        int m = scan.nextInt();
        int[] list = new int[3000005];
        int i = 0;
        while (scan.hasNextInt()) {
            list[i] = scan.nextInt();
            ++i;
        }
        
        int dist = i - m;
        
        if (dist < 0) {
            System.out.println("NIL");
        }
        else {
            System.out.println(list[dist]);
        }
        
    }
}
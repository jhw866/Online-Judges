#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main() {
	int columns;
	while(cin >> columns) {
		int i = 0;
		vector<int> oldBox;
		while(i < columns) {
			int numCubes;
			cin >> numCubes;
			oldBox.push_back(numCubes);
			++i;
		}

		sort(oldBox.begin(), oldBox.end());
		for(i = 0; i < columns; ++i) {
			cout << oldBox.at(i);
			if (i != columns - 1)
				cout << " ";
		}
		cout << endl;
	}
}